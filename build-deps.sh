#!/bin/bash

# run this script with sudo

BUILD_DIR=./build
SRC_DIR=./src
aptresult=""
function install_from_apt_get {
    PACKAGE_NAME=$1
    PACKAGE_INSTALL=$(dpkg-query -W --showformat='${Status}\n' $PACKAGE_NAME|grep "install ok installed")
    if [ -z "$PACKAGE_INSTALL" ]; then
        sudo apt-get install $PACKAGE_NAME
        check_exit_code $PACKAGE_NAME
    else
    	aptresult="${aptresult}$PACKAGE_NAME already installed!\n"
    fi
}
function get_qwt {
    QWT_URL="http://downloads.sourceforge.net/project/qwt/qwt-beta/6.1.0-rc3/qwt-6.1-rc3.tar.bz2"
    wget -O qwt-6.1-rc3.tar.bz2 $QWT_URL
    tar -xjvf qwt-6.1-rc3.tar.bz2
    cd qwt-6.1-rc3
    qmake qwt.pro
    make
    sudo make install
   	check_exit_code qwt-6.1-rc3.tar.bz2
   	((progress+=10));log_progress;
}
function check_exit_code {
	PACKAGE=$1
	if [[ $? != 0 ]] ; then
		zenity --error --text="Install of $PACKAGE failed.\nPlease check $BUILD_DIR/RiTHMInstall.log for details."
		logger -i -p RiTHMInstall.log "Install of $PACKAGE failed with code $?"
		((progress=100));log_progress;
    	exit $?
	else
		echo "message:$PACKAGE succesfully installed." >&3	
		logger -i -t RiTHM -s "$PACKAGE  Installed successfully" 2>> $BUILD_DIR/RiTHMInstall.log
	fi
}
function check_apt_get_deps {
    OS_ARCH=`uname -p`
    if [ "$OS_ARCH" = "x86_64" ]; then
        install_from_apt_get ia32-libs
    fi
	((progress+=7));log_progress;
    install_from_apt_get realpath
    ((progress+=8));log_progress;
    install_from_apt_get subversion
    ((progress+=8));log_progress;
    install_from_apt_get g++
    ((progress+=8));log_progress;
    install_from_apt_get libqt4-core
    ((progress+=8));log_progress;
    install_from_apt_get libqt4-dev
    ((progress+=8));log_progress;
    echo "message:Status\n"$aptresult >&3
}

function unpack_solvers {
    SOLVERS_DIR=$BUILD_DIR/solvers
    mkdir -p $SOLVERS_DIR
    LP_SOLVE=$(find $SOLVERS_DIR -type f -name lp_solve)
    YICES=$(find $SOLVERS_DIR -type f -name yices)
    
    OS_ARCH=`uname -p`

    if [ -z $LP_SOLVE ]; then
        LP_SOLVE_URL="http://downloads.sourceforge.net/project/lpsolve/lpsolve/5.5.2.0/lp_solve_5.5.2.0_exe_ux32.tar.gz?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Flpsolve%2F&ts=1351717641&use_mirror=iweb"
        if [ "$OS_ARCH" = "x86_64" ]; then
            LP_SOLVE_URL="http://downloads.sourceforge.net/project/lpsolve/lpsolve/5.5.2.0/lp_solve_5.5.2.0_exe_ux64.tar.gz?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Flpsolve%2Ffiles%2Flpsolve%2F5.5.2.0%2F&ts=1351717708&use_mirror=iweb"
        fi
        mkdir -p $SOLVERS_DIR/lp_solve
        wget -O $SOLVERS_DIR/lp_solve/lp_solve.tar.gz $LP_SOLVE_URL
        pushd .
        cd $SOLVERS_DIR/lp_solve
        tar -xvzf ./lp_solve.tar.gz
        popd
    fi

    if [ -z $YICES ]; then
        YICES_URL="http://yices.csl.sri.com/cgi-bin/yices-newlicense.cgi?file=yices-1.0.36-i686-pc-linux-gnu-static-gmp.tar.gz"
        if [ "$OS_ARCH" = "x86_64" ]; then
            YICES_URL="http://yices.csl.sri.com/cgi-bin/yices-newlicense.cgi?file=yices-1.0.36-x86_64-pc-linux-gnu-static-gmp.tar.gz"
        fi
        echo "==============="
        echo "SRI requires the user to accept their end-user agreement before downloading Yices."
        echo "Save and untar the archive under $SOLVERS_DIR"
        echo "$YICES_URL"
        echo "==============="
    fi

}

function checkout_llvm {
    ABS_BUILD_DIR=`realpath $BUILD_DIR`
    LLVM_CLANG_VERSION=3.2
    LLVM_DIR=llvm
    LLVM_URL=http://llvm.org/releases/$LLVM_CLANG_VERSION/llvm-$LLVM_CLANG_VERSION.src.tar.gz
    CLANG_URL=http://llvm.org/releases/$LLVM_CLANG_VERSION/clang-$LLVM_CLANG_VERSION.src.tar.gz
    
    if [ ! -d $ABS_BUILD_DIR/$LLVM_DIR ]; then
        echo "Setup LLVM and clang (v.$LLVM_CLANG_VERSION)"
        echo "Target directory: $ABS_BUILD_DIR/$LLVM_DIR"
        mkdir -p $ABS_BUILD_DIR/$LLVM_DIR
        pushd .
        cd $ABS_BUILD_DIR
        wget -O llvm.tar.gz $LLVM_URL
        tar -xvzf llvm.tar.gz
        mv ./llvm-$LLVM_CLANG_VERSION.src/* ./$LLVM_DIR/ 
        rm -f llvm.tar.gz
        rm -rf ./llvm-$LLVM_CLANG_VERSION.src*
        cd $LLVM_DIR/tools
        wget -O clang.tar.gz $CLANG_URL
        tar -xvzf clang.tar.gz
        mv ./clang-$LLVM_CLANG_VERSION.src ./clang
        rm -f clang.tar.gz
        
        mkdir -p $ABS_BUILD_DIR/$LLVM_DIR/build
        cd $ABS_BUILD_DIR/$LLVM_DIR/build
        ../configure
        check_exit_code LLVM
        popd

        pushd .
        cd ./src/llvm
        ./copy-llvm-plugins.sh $ABS_BUILD_DIR/llvm
        popd
    fi

    if [ "build" = "$1" ]; then
        pushd .
        cd $ABS_BUILD_DIR/$LLVM_DIR/build
        make
        check_exit_code LLVM
        popd
    fi
    ((progress+=20));log_progress;
}

function build_libconfig {

    LIBCONFIG_VERSION=1.4.8
    LIBCONFIG=libconfig-$LIBCONFIG_VERSION
    LIBCONFIG_DIR=$BUILD_DIR/$LIBCONFIG
    
    echo "Setup libconfig (v.$LIBCONFIG_VERSION)"
    echo "Target directory: $LIBCONFIG_DIR"
    
    if [ ! -d $LIBCONFIG_DIR ]; then
        echo "$LIBCONFIG_DIR does not exist. Download and configuring libconfig..."
        wget -O $BUILD_DIR/$LIBCONFIG.tar.gz  http://www.hyperrealm.com/libconfig/$LIBCONFIG.tar.gz
        mkdir -p $BUILD_DIR
        pushd .
        cd $BUILD_DIR
        tar -xvzf $LIBCONFIG.tar.gz
        cd $LIBCONFIG
        ./configure
        check_exit_code libconfig
        popd
        rm -f $BUILD_DIR/$LIBCONFIG.tar.gz
    fi
    pushd .
    cd $BUILD_DIR/$LIBCONFIG
    make
    check_exit_code libconfig
    ((progress+=10));log_progress;
    popd
    #libraries will be output to libconfig-1.4.8/lib/.libs
}

function unpack_AMDAPPSDK {
    OS_ARCH=`uname -p`
    AMDAPP_VERSION=2.7
    AMDAPP_SDK="AMD-APP-SDK-v$AMDAPP_VERSION-lnx32"
    AMDAPP_BUILD_DIR=$BUILD_DIR/AMDAPP

    if [ -d /opt/AMDAPP ]; then
        echo "AMD-APP-SDK is already installed."
        return
    fi

    if [ "$OS_ARCH" = "x86_64" ]; then
        AMDAPP_SDK="AMD-APP-SDK-v$AMDAPP_VERSION-lnx64"
    fi

    echo "==============="
    echo "AMD requires the user to accept its license agreement before downloading the AMD APP SDK."
    echo "Save and untar the file. Run sudo ./Install-AMD-APP.sh"
    echo "Download link: http://developer.amd.com.php53-23.ord1-1.websitetestlink.com/wordpress/media/files/$AMDAPP_SDK.tgz"
    echo "==============="
    mkdir -p $AMDAPP_BUILD_DIR/
    wget  -O $AMDAPP_BUILD_DIR/$AMDAPP_SDK.tgz http://developer.amd.com/Downloads/$AMDAPP_SDK.tgz
    pushd .
    cd $AMDAPP_BUILD_DIR
    tar -xvzf $AMDAPP_SDK.tgz
    sudo ./Install-AMD-APP.sh 
    check_exit_code AMDAPP 	
    ((progress+=10));log_progress;
    popd
}
function log_progress {
	echo $progress >> $BUILD_DIR/pbar
}
function prepare_qtgui_makefile {
    ABS_BUILD_DIR=`realpath $BUILD_DIR`
    GUI_BUILD_DIR=$ABS_BUILD_DIR/frontend/gui
    GUI_OBJ_DIR=$GUI_BUILD_DIR/obj/
    GUI_MOC_DIR=$GUI_BUILD_DIR/moc/
    GUI_UI_H_DIR=$GUI_BUILD_DIR/ui/h/
    GUI_OUTPUT_DIR=$ABS_BUILD_DIR
    
    mkdir -p $GUI_BUILD_DIR
    qmake -o $SRC_DIR/frontend/gui/Makefile "OBJECTS_DIR=$GUI_OBJ_DIR"  \
                    "MOC_DIR=$GUI_MOC_DIR"      \
                    "CONFIG += release"     \
                    "DESTDIR = $GUI_OUTPUT_DIR" \
                    "UI_HEADERS_DIR = $GUI_UI_H_DIR" \
                    $SRC_DIR/frontend/gui/rvtool.pro
    check_exit_code RiTHMGUI 	                
} 

function copy_controller_files {
    cd $ABS_BUILD_DIR/../
    mkdir $ABS_BUILD_DIR/monitor
    cp -f -r ./src/monitor/* $ABS_BUILD_DIR/monitor/
}


exec 3> >(zenity --notification --listen)   
echo "message:Starting RiTHM installation" >&3
zenity --text-info \
       --title="RiTHM Installation" \
       --filename="./License" \
       --checkbox="I read and accept the terms."    
case $? in
    0)
        
	# next step
	;;
    1)	echo "message:RiTHM installation aborted.." >&3
    	exit 1
	;;
    -1)
        echo "message:An unexpected error has occurred." >&3
	;;
esac
BUILD_DIR=`zenity --file-selection  --filename="$SRC_DIR" --directory --title="Select a Directory for installation"`

case $? in
         0)
                echo "message:RiTHM will be installed at \"$BUILD_DIR\".\nPlease refer $BUILD_DIR/RiTHMInstall.log for details." >&3;;
         1)
                echo "message:No directory selected.Aborting.." >&3
                exit 1
                ;;
        -1)
                echo "An unexpected error has occurred." >&3 ;;
esac
mkdir -p $BUILD_DIR
touch $BUILD_DIR/pbar
tail -f $BUILD_DIR/pbar| zenity --text=RiTHM Installer --progress --auto-close &
progress=1;log_progress;
check_apt_get_deps
build_libconfig
checkout_llvm build # pass argument 'build' to build LLVM and clang
unpack_AMDAPPSDK

prepare_qtgui_makefile
get_qwt

unpack_solvers
copy_controller_files
((progress+=10));log_progress;
echo "message:RiTHM installed successfully !!" >&3	
logger -i -t RiTHM -s "RiTHM installed successfully at $BUILD_DIR" 2>> $BUILD_DIR/RiTHMInstall.log
#change read/write permissions
sudo chown -R `whoami` ./
