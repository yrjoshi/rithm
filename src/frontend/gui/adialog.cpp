#include "adialog.h"
#include "ui_adialog.h"

ADialog::ADialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ADialog)
{
    ui->setupUi(this);
}

ADialog::~ADialog()
{
    delete ui;
}

void ADialog::on_pushButton_clicked()
{
    this->setVisible(FALSE);
}
