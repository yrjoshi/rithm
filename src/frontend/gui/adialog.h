#ifndef ADIALOG_H
#define ADIALOG_H

#include <QDialog>
#include "ui_adialog.h"
namespace Ui {
class ADialog;
}

class ADialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ADialog(QWidget *parent = 0);
    ~ADialog();
    
private slots:
    void on_pushButton_clicked();

private:
    Ui::ADialog *ui;
};

#endif // ADIALOG_H
