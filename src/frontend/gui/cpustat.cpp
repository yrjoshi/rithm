#include <qstringlist.h>
#include <qfile.h>
#include <qtextstream.h>
#include "cpustat.h"
#include <QDebug>

MonitorStat::MonitorStat()
{
    lookUp( procValues );
}

QTime MonitorStat::upTime() const
{
    QTime t( 0, 0, 0 );
    for ( int i = 0; i < NValues; i++ )
        t = t.addSecs( int( procValues[i] / 100 ) );

    return t;
}

void MonitorStat::statistic( double &pp, double &buffer )
{
    double values[NValues];

    lookUp( values );

    pp = values[PollingPeriod];
    buffer = values[Buffer];

    /*double userDelta = values[User] + values[Nice]
        - procValues[User] - procValues[Nice];
    double systemDelta = values[System] - procValues[System];

    double totalDelta = 0;
    for ( int i = 0; i < NValues; i++ )
        totalDelta += values[i] - procValues[i];

    user = userDelta / totalDelta * 100.0;
    system = systemDelta / totalDelta * 100.0;

    for ( int j = 0; j < NValues; j++ )
        procValues[j] = values[j];*/
}

void MonitorStat::lookUp( double values[NValues] ) const
{
    QFile file( "/tmp/rithm" );
    if ( !file.open( QIODevice::ReadOnly ) )
    {
        values[0] = 0;
        values[1] = 0;
    }
    else
    {
        QTextStream textStream( &file );

        {
            QString line = textStream.readLine();
            line = line.trimmed();
            qDebug() << "Line" << line;
            try
            {
            const QStringList valueList =
                    line.split( "\t",  QString::SkipEmptyParts );
            values[0] = valueList[0].toDouble();
            values[1] = valueList[1].toInt();
            }
            catch(int e)
            {
                values[0] = 0;
                values[1] = 0;
            }
        }
    }
}
