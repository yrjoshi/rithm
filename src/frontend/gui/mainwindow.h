#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QToolBar>
#include <qwt_plot_curve.h>
#include "settingswindow.h"
#include "cpuplot.h"
#include "aboutdialog.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    void timerEvent( QTimerEvent * );
    ~MainWindow();

private slots:
       void BrowseSrcDir();
       void BrowseOutputDir();
       void BrowsePropFilepath();
       void RunTool();
       void LaunchSettingsWindow();
       void showabout(QAction*);
       void on_pushButton_clicked();
       void DynamicChanged(int index);
       void ControllerChanged(const QString &arg1);
       void FixedToggled(bool checked);
       void DynamicToggled(bool checked);

       void on_pushButtonViewLog_clicked();

       void on_tabWidget_selected(const QString &arg1);
       void on_comboBoxArch_currentIndexChanged(const QString &arg1);

       void on_CPUtimebutton_clicked();

       void on_CPUtimebutton_2_clicked();

private:
    enum SolveType {
        SOLVE_ILP = 0,
        SOLVE_H1,
        SOLVE_H2,
        SOLVE_SAT,
        SOLVE_GREEDY,
        SOLVE_TYPE_MAX
    };

    static const QString SolveTypeArg[SOLVE_TYPE_MAX];
    static const QString GooMFAlgType[4];
    static const QString GooMFInvokeType[2];

    Ui::MainWindow *ui;
    QAction *about;
    QAction *settings;
    QAction *run;
    MonitorPlot *plot;
    AboutDialog *aboutd;
    QTime plottime;
    QwtPlotCurve *cpu_m;
    QwtPlotCurve *cpu_a;
    QVector<QPointF> points;
    QVector<QPointF> points_a;
    QString oldsrcdir;
    QString oldoutdir;
    int mytimer;
};

#endif // MAINWINDOW_H
