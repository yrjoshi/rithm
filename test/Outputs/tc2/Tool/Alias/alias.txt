Alias Set Tracker: 7 alias sets for 7 pointer values.
  AliasSet[0x31e52c0, 1] must alias, Mod       Pointers: (i32* %retval, 4)
  AliasSet[0x31e5310, 2] may alias, Mod/Ref   Pointers: (i32* @main_n, 4), (i32* @main_i, 4)
  AliasSet[0x31e53d0, 1] must alias, Mod/Ref   Pointers: (i32* %Fnew, 4)
  AliasSet[0x31e5460, 1] must alias, Mod/Ref   Pointers: (i32* %Fold, 4)
  AliasSet[0x31e54f0, 1] must alias, Mod/Ref    forwarding to 0x31e5310
  AliasSet[0x31e5580, 1] must alias, Mod/Ref   Pointers: (i32* %temp, 4)
  AliasSet[0x31e5660, 1] must alias, Mod/Ref   Pointers: (i32* %ans, 4)
