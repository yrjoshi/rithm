; ModuleID = 'RiTHMicMonitor.c'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct._GOOMF_context_struct = type opaque
%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct._GOOMF_host_program_state_struct = type { i32, i32, i32 }
%struct.cpu_set_t = type { [16 x i64] }
%struct._program_thread_args = type { i32, i8** }
%union.pthread_attr_t = type { i64, [48 x i8] }
%struct.timeval = type { i64, i64 }
%struct.timespec = type { i64, i64 }
%struct.sched_param = type { i32 }

@context = global %struct._GOOMF_context_struct* null, align 8
@logFile = external global %struct._IO_FILE*
@.str = private unnamed_addr constant [11 x i8] c"prop1: %s\0A\00", align 1
@kill_monitor = global i32 0, align 4
@.str1 = private unnamed_addr constant [34 x i8] c"Cannot set affinity to monitor..\0A\00", align 1
@.str2 = private unnamed_addr constant [6 x i8] c"-test\00", align 1
@.str3 = private unnamed_addr constant [4 x i8] c"%s\0A\00", align 1
@.str4 = private unnamed_addr constant [23 x i8] c"Context initialized..\0A\00", align 1
@.str5 = private unnamed_addr constant [39 x i8] c"Problem with context initialization..\0A\00", align 1
@binary_search_low = external global i32
@current_program_state = common global %struct._GOOMF_host_program_state_struct zeroinitializer, align 4
@binary_search_up = external global i32
@binary_search_mid = external global i32
@.str6 = private unnamed_addr constant [8 x i8] c"monitor\00", align 1
@.str7 = private unnamed_addr constant [15 x i8] c"Average is %f\0A\00", align 1
@.str8 = private unnamed_addr constant [21 x i8] c"Super Average is %f\0A\00", align 1
@stderr = external global %struct._IO_FILE*
@.str9 = private unnamed_addr constant [42 x i8] c"main_thread() malloc failed! Aborting...\0A\00", align 1

define i32 @initContext() nounwind uwtable {
  %1 = call i32 @_GOOMF_initContext(%struct._GOOMF_context_struct** @context, i32 0, i32 0, i32 0, i32 200)
  ret i32 %1
}

declare i32 @_GOOMF_initContext(%struct._GOOMF_context_struct**, i32, i32, i32, i32)

define void @report() nounwind uwtable {
  %v = alloca [1 x i32], align 4
  %str = alloca [100 x i8], align 16
  %i = alloca i32, align 4
  %1 = load %struct._GOOMF_context_struct** @context, align 8
  %2 = getelementptr inbounds [1 x i32]* %v, i32 0, i32 0
  %3 = call i32 @_GOOMF_getCurrentStatus(%struct._GOOMF_context_struct* %1, i32* %2)
  %4 = getelementptr inbounds [1 x i32]* %v, i32 0, i64 0
  %5 = getelementptr inbounds [100 x i8]* %str, i32 0, i32 0
  %6 = call i32 @_GOOMF_typeToString(i32* %4, i8* %5)
  %7 = load %struct._IO_FILE** @logFile, align 8
  %8 = getelementptr inbounds [100 x i8]* %str, i32 0, i32 0
  %9 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([11 x i8]* @.str, i32 0, i32 0), i8* %8)
  ret void
}

declare i32 @_GOOMF_getCurrentStatus(%struct._GOOMF_context_struct*, i32*)

declare i32 @_GOOMF_typeToString(i32*, i8*)

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...)

define i32 @destroyContext() nounwind uwtable {
  %1 = call i32 @_GOOMF_destroyContext(%struct._GOOMF_context_struct** @context)
  ret i32 %1
}

declare i32 @_GOOMF_destroyContext(%struct._GOOMF_context_struct**)

define i32 @main(i32 %argc, i8** %argv) nounwind uwtable {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 8
  %thread1 = alloca i64, align 8
  %thread2 = alloca i64, align 8
  %mask = alloca %struct.cpu_set_t, align 8
  %args = alloca %struct._program_thread_args*, align 8
  %__cpu = alloca i64, align 8
  %result = alloca i32, align 4
  store i32 0, i32* %1
  store i32 %argc, i32* %2, align 4
  store i8** %argv, i8*** %3, align 8
  %4 = call noalias i8* @malloc(i64 16) nounwind
  %5 = bitcast i8* %4 to %struct._program_thread_args*
  store %struct._program_thread_args* %5, %struct._program_thread_args** %args, align 8
  %6 = load i32* %2, align 4
  %7 = load %struct._program_thread_args** %args, align 8
  %8 = getelementptr inbounds %struct._program_thread_args* %7, i32 0, i32 0
  store i32 %6, i32* %8, align 4
  %9 = load i8*** %3, align 8
  %10 = load %struct._program_thread_args** %args, align 8
  %11 = getelementptr inbounds %struct._program_thread_args* %10, i32 0, i32 1
  store i8** %9, i8*** %11, align 8
  br label %12

; <label>:12                                      ; preds = %0
  %13 = bitcast %struct.cpu_set_t* %mask to i8*
  call void @llvm.memset.p0i8.i64(i8* %13, i8 0, i64 128, i32 1, i1 false)
  br label %14

; <label>:14                                      ; preds = %12
  store i64 0, i64* %__cpu, align 8
  %15 = load i64* %__cpu, align 8
  %16 = icmp ult i64 %15, 1024
  br i1 %16, label %17, label %28

; <label>:17                                      ; preds = %14
  %18 = load i64* %__cpu, align 8
  %19 = urem i64 %18, 64
  %20 = shl i64 1, %19
  %21 = load i64* %__cpu, align 8
  %22 = udiv i64 %21, 64
  %23 = getelementptr inbounds %struct.cpu_set_t* %mask, i32 0, i32 0
  %24 = getelementptr inbounds [16 x i64]* %23, i32 0, i32 0
  %25 = getelementptr inbounds i64* %24, i64 %22
  %26 = load i64* %25
  %27 = or i64 %26, %20
  store i64 %27, i64* %25
  br label %29

; <label>:28                                      ; preds = %14
  br label %29

; <label>:29                                      ; preds = %28, %17
  %30 = phi i64 [ %27, %17 ], [ 0, %28 ]
  %31 = call i32 @sched_setaffinity(i32 0, i64 128, %struct.cpu_set_t* %mask) nounwind
  store i32 %31, i32* %result, align 4
  %32 = load i32* %result, align 4
  %33 = icmp ne i32 %32, 0
  br i1 %33, label %34, label %36

; <label>:34                                      ; preds = %29
  %35 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([34 x i8]* @.str1, i32 0, i32 0))
  call void @exit(i32 0) noreturn nounwind
  unreachable

; <label>:36                                      ; preds = %29
  %37 = load i32* %2, align 4
  %38 = icmp sge i32 %37, 2
  br i1 %38, label %39, label %50

; <label>:39                                      ; preds = %36
  %40 = load i8*** %3, align 8
  %41 = getelementptr inbounds i8** %40, i64 1
  %42 = load i8** %41
  %43 = call i32 @strcmp(i8* %42, i8* getelementptr inbounds ([6 x i8]* @.str2, i32 0, i32 0)) nounwind readonly
  %44 = icmp ne i32 %43, 0
  br i1 %44, label %50, label %45

; <label>:45                                      ; preds = %39
  %46 = load i8*** %3, align 8
  %47 = getelementptr inbounds i8** %46, i64 1
  %48 = load i8** %47
  %49 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @.str3, i32 0, i32 0), i8* %48)
  br label %59

; <label>:50                                      ; preds = %39, %36
  %51 = call i32 @pthread_create(i64* %thread1, %union.pthread_attr_t* null, i8* (i8*)* bitcast (void ()* @monitor_thread to i8* (i8*)*), i8* null) nounwind
  %52 = load %struct._program_thread_args** %args, align 8
  %53 = bitcast %struct._program_thread_args* %52 to i8*
  %54 = call i32 @pthread_create(i64* %thread2, %union.pthread_attr_t* null, i8* (i8*)* bitcast (void (i8*)* @main_thread to i8* (i8*)*), i8* %53) nounwind
  %55 = load i64* %thread1, align 8
  %56 = call i32 @pthread_join(i64 %55, i8** null)
  %57 = load i64* %thread2, align 8
  %58 = call i32 @pthread_join(i64 %57, i8** null)
  br label %59

; <label>:59                                      ; preds = %50, %45
  call void @report()
  %60 = call i32 @destroyContext()
  %61 = load %struct._program_thread_args** %args, align 8
  %62 = bitcast %struct._program_thread_args* %61 to i8*
  call void @free(i8* %62) nounwind
  call void @exit(i32 0) noreturn nounwind
  unreachable
                                                  ; No predecessors!
  %64 = load i32* %1
  ret i32 %64
}

declare noalias i8* @malloc(i64) nounwind

declare void @llvm.memset.p0i8.i64(i8* nocapture, i8, i64, i32, i1) nounwind

declare i32 @sched_setaffinity(i32, i64, %struct.cpu_set_t*) nounwind

declare i32 @printf(i8*, ...)

declare void @exit(i32) noreturn nounwind

declare i32 @strcmp(i8*, i8*) nounwind readonly

declare i32 @pthread_create(i64*, %union.pthread_attr_t*, i8* (i8*)*, i8*) nounwind

declare i32 @pthread_join(i64, i8**)

declare void @free(i8*) nounwind

define void @monitor_thread() nounwind uwtable {
  %start = alloca %struct.timeval, align 8
  %end = alloca %struct.timeval, align 8
  %sleep_time = alloca %struct.timespec, align 8
  %start_nano = alloca %struct.timespec, align 8
  %end_nano = alloca %struct.timespec, align 8
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %param = alloca %struct.sched_param, align 4
  %policy = alloca i32, align 4
  store i32 0, i32* %i, align 4
  store i32 0, i32* %j, align 4
  store i32 1, i32* %policy, align 4
  %1 = call i64 @pthread_self() nounwind readnone
  %2 = load i32* %policy, align 4
  %3 = call i32 @pthread_setschedparam(i64 %1, i32 %2, %struct.sched_param* %param) nounwind
  %4 = call i32 @mlockall(i32 3) nounwind
  %5 = getelementptr inbounds %struct.timespec* %sleep_time, i32 0, i32 0
  store i64 0, i64* %5, align 8
  %6 = getelementptr inbounds %struct.timespec* %sleep_time, i32 0, i32 1
  store i64 19, i64* %6, align 8
  %7 = call i32 @initContext()
  %8 = icmp eq i32 %7, 0
  br i1 %8, label %9, label %11

; <label>:9                                       ; preds = %0
  %10 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([23 x i8]* @.str4, i32 0, i32 0))
  br label %13

; <label>:11                                      ; preds = %0
  %12 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([39 x i8]* @.str5, i32 0, i32 0))
  br label %13

; <label>:13                                      ; preds = %11, %9
  br label %14

; <label>:14                                      ; preds = %18, %13
  %15 = load i32* @kill_monitor, align 4
  %16 = icmp ne i32 %15, 0
  %17 = xor i1 %16, true
  br i1 %17, label %18, label %32

; <label>:18                                      ; preds = %14
  %19 = call i32 @clock_gettime(i32 0, %struct.timespec* %start_nano) nounwind
  %20 = call i32 @clock_nanosleep(i32 0, i32 0, %struct.timespec* %sleep_time, %struct.timespec* null)
  %21 = load i32* @binary_search_low, align 4
  store i32 %21, i32* getelementptr inbounds (%struct._GOOMF_host_program_state_struct* @current_program_state, i32 0, i32 0), align 4
  %22 = load i32* @binary_search_up, align 4
  store i32 %22, i32* getelementptr inbounds (%struct._GOOMF_host_program_state_struct* @current_program_state, i32 0, i32 1), align 4
  %23 = load i32* @binary_search_mid, align 4
  store i32 %23, i32* getelementptr inbounds (%struct._GOOMF_host_program_state_struct* @current_program_state, i32 0, i32 2), align 4
  %24 = load %struct._GOOMF_context_struct** @context, align 8
  %25 = call i32 @_GOOMF_nextState(%struct._GOOMF_context_struct* %24, i8* bitcast (%struct._GOOMF_host_program_state_struct* @current_program_state to i8*))
  %26 = load i64* getelementptr ({ i64, i32 }* bitcast (%struct._GOOMF_host_program_state_struct* @current_program_state to { i64, i32 }*), i32 0, i32 0), align 1
  %27 = load i32* getelementptr ({ i64, i32 }* bitcast (%struct._GOOMF_host_program_state_struct* @current_program_state to { i64, i32 }*), i32 0, i32 1), align 1
  %28 = call i32 (...)* @logevent(i8* getelementptr inbounds ([8 x i8]* @.str6, i32 0, i32 0), i32 0, i64 %26, i32 %27)
  %29 = load %struct._GOOMF_context_struct** @context, align 8
  %30 = call i32 @_GOOMF_flush(%struct._GOOMF_context_struct* %29)
  %31 = call i32 @clock_gettime(i32 0, %struct.timespec* %end_nano) nounwind
  br label %14

; <label>:32                                      ; preds = %14
  call void @pthread_exit(i8* null) noreturn
  unreachable
                                                  ; No predecessors!
  ret void
}

declare i32 @pthread_setschedparam(i64, i32, %struct.sched_param*) nounwind

declare i64 @pthread_self() nounwind readnone

declare i32 @mlockall(i32) nounwind

declare i32 @clock_gettime(i32, %struct.timespec*) nounwind

declare i32 @clock_nanosleep(i32, i32, %struct.timespec*, %struct.timespec*)

declare i32 @_GOOMF_nextState(%struct._GOOMF_context_struct*, i8*)

declare i32 @logevent(...)

declare i32 @_GOOMF_flush(%struct._GOOMF_context_struct*)

declare void @pthread_exit(i8*) noreturn

define void @checktimers() nounwind uwtable {
  %start = alloca %struct.timeval, align 8
  %end = alloca %struct.timeval, align 8
  %sleep_time = alloca %struct.timespec, align 8
  %start_nano = alloca %struct.timespec, align 8
  %end_nano = alloca %struct.timespec, align 8
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %param = alloca %struct.sched_param, align 4
  %policy = alloca i32, align 4
  %super_average = alloca double, align 8
  %average = alloca double, align 8
  %ret = alloca i32, align 4
  store i32 0, i32* %i, align 4
  store i32 0, i32* %j, align 4
  store i32 1, i32* %policy, align 4
  %1 = call i32 @sched_get_priority_max(i32 1) nounwind
  %2 = getelementptr inbounds %struct.sched_param* %param, i32 0, i32 0
  store i32 %1, i32* %2, align 4
  %3 = load i32* %policy, align 4
  %4 = call i32 @sched_setscheduler(i32 0, i32 %3, %struct.sched_param* %param) nounwind
  %5 = call i32 @mlockall(i32 3) nounwind
  %6 = getelementptr inbounds %struct.timespec* %sleep_time, i32 0, i32 0
  store i64 0, i64* %6, align 8
  %7 = getelementptr inbounds %struct.timespec* %sleep_time, i32 0, i32 1
  store i64 5000, i64* %7, align 8
  store double 0.000000e+00, double* %super_average, align 8
  store i32 0, i32* %j, align 4
  br label %8

; <label>:8                                       ; preds = %45, %0
  %9 = load i32* %j, align 4
  %10 = icmp slt i32 %9, 20
  br i1 %10, label %11, label %48

; <label>:11                                      ; preds = %8
  store double 0.000000e+00, double* %average, align 8
  store i32 0, i32* %i, align 4
  br label %12

; <label>:12                                      ; preds = %27, %11
  %13 = load i32* %i, align 4
  %14 = icmp slt i32 %13, 1000
  br i1 %14, label %15, label %30

; <label>:15                                      ; preds = %12
  %16 = call i32 @clock_gettime(i32 0, %struct.timespec* %start_nano) nounwind
  %17 = call i32 @clock_nanosleep(i32 0, i32 0, %struct.timespec* %sleep_time, %struct.timespec* null)
  store i32 %17, i32* %ret, align 4
  %18 = call i32 @clock_gettime(i32 0, %struct.timespec* %end_nano) nounwind
  %19 = getelementptr inbounds %struct.timespec* %end_nano, i32 0, i32 1
  %20 = load i64* %19, align 8
  %21 = getelementptr inbounds %struct.timespec* %start_nano, i32 0, i32 1
  %22 = load i64* %21, align 8
  %23 = sub nsw i64 %20, %22
  %24 = sitofp i64 %23 to double
  %25 = load double* %average, align 8
  %26 = fadd double %25, %24
  store double %26, double* %average, align 8
  br label %27

; <label>:27                                      ; preds = %15
  %28 = load i32* %i, align 4
  %29 = add nsw i32 %28, 1
  store i32 %29, i32* %i, align 4
  br label %12

; <label>:30                                      ; preds = %12
  %31 = load double* %average, align 8
  %32 = fcmp olt double %31, 0.000000e+00
  br i1 %32, label %33, label %36

; <label>:33                                      ; preds = %30
  %34 = load i32* %j, align 4
  %35 = add nsw i32 %34, -1
  store i32 %35, i32* %j, align 4
  br label %45

; <label>:36                                      ; preds = %30
  %37 = load double* %average, align 8
  %38 = fdiv double %37, 1.000000e+03
  store double %38, double* %average, align 8
  %39 = load double* %average, align 8
  %40 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str7, i32 0, i32 0), double %39)
  %41 = load double* %average, align 8
  %42 = load double* %super_average, align 8
  %43 = fadd double %42, %41
  store double %43, double* %super_average, align 8
  %44 = call i32 @usleep(i32 200000)
  br label %45

; <label>:45                                      ; preds = %36, %33
  %46 = load i32* %j, align 4
  %47 = add nsw i32 %46, 1
  store i32 %47, i32* %j, align 4
  br label %8

; <label>:48                                      ; preds = %8
  %49 = load double* %super_average, align 8
  %50 = fdiv double %49, 2.000000e+01
  %51 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([21 x i8]* @.str8, i32 0, i32 0), double %50)
  ret void
}

declare i32 @sched_get_priority_max(i32) nounwind

declare i32 @sched_setscheduler(i32, i32, %struct.sched_param*) nounwind

declare i32 @usleep(i32)

define void @main_thread(i8* %program_main_data) nounwind uwtable {
  %1 = alloca i8*, align 8
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %y = alloca i32, align 4
  %param = alloca %struct.sched_param, align 4
  %args = alloca %struct._program_thread_args*, align 8
  %policy = alloca i32, align 4
  %retVal = alloca i32*, align 8
  store i8* %program_main_data, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = bitcast i8* %2 to %struct._program_thread_args*
  store %struct._program_thread_args* %3, %struct._program_thread_args** %args, align 8
  store i32 0, i32* %policy, align 4
  %4 = call i64 @pthread_self() nounwind readnone
  %5 = load i32* %policy, align 4
  %6 = call i32 @pthread_setschedparam(i64 %4, i32 %5, %struct.sched_param* %param) nounwind
  %7 = call noalias i8* @malloc(i64 4) nounwind
  %8 = bitcast i8* %7 to i32*
  store i32* %8, i32** %retVal, align 8
  %9 = load i32** %retVal, align 8
  %10 = icmp ne i32* %9, null
  br i1 %10, label %14, label %11

; <label>:11                                      ; preds = %0
  %12 = load %struct._IO_FILE** @stderr, align 8
  %13 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([42 x i8]* @.str9, i32 0, i32 0))
  call void @pthread_exit(i8* null) noreturn
  unreachable

; <label>:14                                      ; preds = %0
  %15 = load i32** %retVal, align 8
  store i32 0, i32* %15
  %16 = call i32 (...)* @program_main()
  store i32 1, i32* @kill_monitor, align 4
  %17 = load i32** %retVal, align 8
  %18 = bitcast i32* %17 to i8*
  call void @pthread_exit(i8* %18) noreturn
  unreachable
                                                  ; No predecessors!
  ret void
}

declare i32 @program_main(...)
