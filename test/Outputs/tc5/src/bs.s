; ModuleID = 'bs.c'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct.DATA = type { i32, i32 }
%struct._GOOMF_host_program_state_struct = type { i32, i32, i32 }
%struct._GOOMF_context_struct = type opaque

@logFile = external global %struct._IO_FILE*
@.str = private unnamed_addr constant [21 x i8] c"/tmp/rithm_trace.log\00", align 1
@.str1 = private unnamed_addr constant [2 x i8] c"w\00", align 1
@.str2 = private unnamed_addr constant [83 x i8] c"\0A________________________________________________________________________________\0A\00", align 1
@.str3 = private unnamed_addr constant [48 x i8] c"line_num,event.low,event.up,event.mid,File Name\00", align 1
@.str4 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str5 = private unnamed_addr constant [17 x i8] c"%ld, %d, %d, %d,\00", align 1
@.str6 = private unnamed_addr constant [8 x i8] c"In %s \0A\00", align 1
@data = global [15 x %struct.DATA] [%struct.DATA { i32 1, i32 100 }, %struct.DATA { i32 5, i32 200 }, %struct.DATA { i32 6, i32 300 }, %struct.DATA { i32 7, i32 700 }, %struct.DATA { i32 8, i32 900 }, %struct.DATA { i32 9, i32 250 }, %struct.DATA { i32 10, i32 400 }, %struct.DATA { i32 11, i32 600 }, %struct.DATA { i32 12, i32 800 }, %struct.DATA { i32 13, i32 1500 }, %struct.DATA { i32 14, i32 1200 }, %struct.DATA { i32 15, i32 110 }, %struct.DATA { i32 16, i32 140 }, %struct.DATA { i32 17, i32 133 }, %struct.DATA { i32 18, i32 10 }], align 16
@binary_search_low = external global i32
@current_program_state = common global %struct._GOOMF_host_program_state_struct zeroinitializer, align 4
@context = external global %struct._GOOMF_context_struct*
@.str7 = private unnamed_addr constant [5 x i8] c"main\00", align 1
@binary_search_up = external global i32
@binary_search_mid = external global i32
@.str8 = private unnamed_addr constant [9 x i8] c"FOUND!!\0A\00", align 1
@.str9 = private unnamed_addr constant [7 x i8] c"MID-1\0A\00", align 1
@.str10 = private unnamed_addr constant [7 x i8] c"MID+1\0A\00", align 1
@cnt1 = common global i32 0, align 4
@.str11 = private unnamed_addr constant [17 x i8] c"Loop Count : %d\0A\00", align 1

define void @logevent(i8* %file, i64 %line_num, i64 %event.coerce0, i32 %event.coerce1) nounwind uwtable {
  %1 = alloca i8*, align 8
  %2 = alloca i64, align 8
  %event = alloca %struct._GOOMF_host_program_state_struct, align 8
  store i8* %file, i8** %1, align 8
  store i64 %line_num, i64* %2, align 8
  %3 = bitcast %struct._GOOMF_host_program_state_struct* %event to { i64, i32 }*
  %4 = getelementptr { i64, i32 }* %3, i32 0, i32 0
  store i64 %event.coerce0, i64* %4
  %5 = getelementptr { i64, i32 }* %3, i32 0, i32 1
  store i32 %event.coerce1, i32* %5
  %6 = load %struct._IO_FILE** @logFile, align 8
  %7 = icmp ne %struct._IO_FILE* %6, null
  br i1 %7, label %10, label %8

; <label>:8                                       ; preds = %0
  %9 = call %struct._IO_FILE* @fopen(i8* getelementptr inbounds ([21 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8]* @.str1, i32 0, i32 0))
  store %struct._IO_FILE* %9, %struct._IO_FILE** @logFile, align 8
  br label %10

; <label>:10                                      ; preds = %8, %0
  %11 = load %struct._IO_FILE** @logFile, align 8
  %12 = icmp ne %struct._IO_FILE* %11, null
  br i1 %12, label %13, label %32

; <label>:13                                      ; preds = %10
  %14 = load %struct._IO_FILE** @logFile, align 8
  %15 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([83 x i8]* @.str2, i32 0, i32 0))
  %16 = load %struct._IO_FILE** @logFile, align 8
  %17 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([48 x i8]* @.str3, i32 0, i32 0))
  %18 = load %struct._IO_FILE** @logFile, align 8
  %19 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([2 x i8]* @.str4, i32 0, i32 0))
  %20 = load %struct._IO_FILE** @logFile, align 8
  %21 = load i64* %2, align 8
  %22 = getelementptr inbounds %struct._GOOMF_host_program_state_struct* %event, i32 0, i32 0
  %23 = load i32* %22, align 4
  %24 = getelementptr inbounds %struct._GOOMF_host_program_state_struct* %event, i32 0, i32 1
  %25 = load i32* %24, align 4
  %26 = getelementptr inbounds %struct._GOOMF_host_program_state_struct* %event, i32 0, i32 2
  %27 = load i32* %26, align 4
  %28 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %20, i8* getelementptr inbounds ([17 x i8]* @.str5, i32 0, i32 0), i64 %21, i32 %23, i32 %25, i32 %27)
  %29 = load %struct._IO_FILE** @logFile, align 8
  %30 = load i8** %1, align 8
  %31 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([8 x i8]* @.str6, i32 0, i32 0), i8* %30)
  br label %32

; <label>:32                                      ; preds = %13, %10
  ret void
}

declare %struct._IO_FILE* @fopen(i8*, i8*)

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...)

define void @program_main() nounwind uwtable {
  %1 = call i32 @binary_search(i32 8)
  ret void
}

define i32 @binary_search(i32 %x) nounwind uwtable {
  %1 = alloca i32, align 4
  %fvalue = alloca i32, align 4
  store i32 %x, i32* %1, align 4
  store i32 0, i32* @binary_search_low, align 4
  %2 = load i32* @binary_search_low, align 4
  store i32 %2, i32* getelementptr inbounds (%struct._GOOMF_host_program_state_struct* @current_program_state, i32 0, i32 0), align 4
  %3 = load %struct._GOOMF_context_struct** @context, align 8
  %4 = call i32 @_GOOMF_nextEvent(%struct._GOOMF_context_struct* %3, i32 0, i8* bitcast (i32* @binary_search_low to i8*))
  %5 = load i64* getelementptr ({ i64, i32 }* bitcast (%struct._GOOMF_host_program_state_struct* @current_program_state to { i64, i32 }*), i32 0, i32 0), align 1
  %6 = load i32* getelementptr ({ i64, i32 }* bitcast (%struct._GOOMF_host_program_state_struct* @current_program_state to { i64, i32 }*), i32 0, i32 1), align 1
  call void @logevent(i8* getelementptr inbounds ([5 x i8]* @.str7, i32 0, i32 0), i64 85, i64 %5, i32 %6)
  store i32 14, i32* @binary_search_up, align 4
  %7 = load i32* @binary_search_up, align 4
  store i32 %7, i32* getelementptr inbounds (%struct._GOOMF_host_program_state_struct* @current_program_state, i32 0, i32 1), align 4
  %8 = load %struct._GOOMF_context_struct** @context, align 8
  %9 = call i32 @_GOOMF_nextEvent(%struct._GOOMF_context_struct* %8, i32 1, i8* bitcast (i32* @binary_search_up to i8*))
  %10 = load i64* getelementptr ({ i64, i32 }* bitcast (%struct._GOOMF_host_program_state_struct* @current_program_state to { i64, i32 }*), i32 0, i32 0), align 1
  %11 = load i32* getelementptr ({ i64, i32 }* bitcast (%struct._GOOMF_host_program_state_struct* @current_program_state to { i64, i32 }*), i32 0, i32 1), align 1
  call void @logevent(i8* getelementptr inbounds ([5 x i8]* @.str7, i32 0, i32 0), i64 86, i64 %10, i32 %11)
  store i32 -1, i32* %fvalue, align 4
  br label %12

; <label>:12                                      ; preds = %60, %0
  %13 = load i32* @binary_search_low, align 4
  %14 = load i32* @binary_search_up, align 4
  %15 = icmp sle i32 %13, %14
  br i1 %15, label %16, label %63

; <label>:16                                      ; preds = %12
  %17 = load i32* @binary_search_low, align 4
  %18 = load i32* @binary_search_up, align 4
  %19 = add nsw i32 %17, %18
  %20 = ashr i32 %19, 1
  store i32 %20, i32* @binary_search_mid, align 4
  %21 = load i32* @binary_search_mid, align 4
  store i32 %21, i32* getelementptr inbounds (%struct._GOOMF_host_program_state_struct* @current_program_state, i32 0, i32 2), align 4
  %22 = load %struct._GOOMF_context_struct** @context, align 8
  %23 = call i32 @_GOOMF_nextEvent(%struct._GOOMF_context_struct* %22, i32 2, i8* bitcast (i32* @binary_search_mid to i8*))
  %24 = load i64* getelementptr ({ i64, i32 }* bitcast (%struct._GOOMF_host_program_state_struct* @current_program_state to { i64, i32 }*), i32 0, i32 0), align 1
  %25 = load i32* getelementptr ({ i64, i32 }* bitcast (%struct._GOOMF_host_program_state_struct* @current_program_state to { i64, i32 }*), i32 0, i32 1), align 1
  call void @logevent(i8* getelementptr inbounds ([5 x i8]* @.str7, i32 0, i32 0), i64 89, i64 %24, i32 %25)
  %26 = call i32 (...)* @usleep(i32 1000)
  %27 = load i32* @binary_search_mid, align 4
  %28 = sext i32 %27 to i64
  %29 = getelementptr inbounds [15 x %struct.DATA]* @data, i32 0, i64 %28
  %30 = getelementptr inbounds %struct.DATA* %29, i32 0, i32 0
  %31 = load i32* %30, align 4
  %32 = load i32* %1, align 4
  %33 = icmp eq i32 %31, %32
  br i1 %33, label %34, label %43

; <label>:34                                      ; preds = %16
  %35 = load i32* @binary_search_low, align 4
  %36 = sub nsw i32 %35, 1
  store i32 %36, i32* @binary_search_up, align 4
  %37 = load i32* @binary_search_mid, align 4
  %38 = sext i32 %37 to i64
  %39 = getelementptr inbounds [15 x %struct.DATA]* @data, i32 0, i64 %38
  %40 = getelementptr inbounds %struct.DATA* %39, i32 0, i32 1
  %41 = load i32* %40, align 4
  store i32 %41, i32* %fvalue, align 4
  %42 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([9 x i8]* @.str8, i32 0, i32 0))
  br label %60

; <label>:43                                      ; preds = %16
  %44 = load i32* @binary_search_mid, align 4
  %45 = sext i32 %44 to i64
  %46 = getelementptr inbounds [15 x %struct.DATA]* @data, i32 0, i64 %45
  %47 = getelementptr inbounds %struct.DATA* %46, i32 0, i32 0
  %48 = load i32* %47, align 4
  %49 = load i32* %1, align 4
  %50 = icmp sgt i32 %48, %49
  br i1 %50, label %51, label %55

; <label>:51                                      ; preds = %43
  %52 = load i32* @binary_search_mid, align 4
  %53 = sub nsw i32 %52, 1
  store i32 %53, i32* @binary_search_up, align 4
  %54 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([7 x i8]* @.str9, i32 0, i32 0))
  br label %59

; <label>:55                                      ; preds = %43
  %56 = load i32* @binary_search_mid, align 4
  %57 = add nsw i32 %56, 1
  store i32 %57, i32* @binary_search_low, align 4
  %58 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([7 x i8]* @.str10, i32 0, i32 0))
  br label %59

; <label>:59                                      ; preds = %55, %51
  br label %60

; <label>:60                                      ; preds = %59, %34
  %61 = load i32* @cnt1, align 4
  %62 = add nsw i32 %61, 1
  store i32 %62, i32* @cnt1, align 4
  br label %12

; <label>:63                                      ; preds = %12
  %64 = load i32* @cnt1, align 4
  %65 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([17 x i8]* @.str11, i32 0, i32 0), i32 %64)
  %66 = load i32* %fvalue, align 4
  ret i32 %66
}

declare i32 @_GOOMF_nextEvent(%struct._GOOMF_context_struct*, i32, i8*)

declare i32 @usleep(...)

declare i32 @printf(i8*, ...)
