#include "rithmic_globals.h"


#define SWAP(a,b) sort_temp=(a);(a)=(b);(b)=sort_temp;
#define M 7
#define NSTACK 50

int arr[20] = {
  5, 4, 10, 1, 5, 100, 231, 111, 49, 99,
  10, 150, 222, 101, 77, 44, 35, 20, 99, 88
};

int istack[100];

extern unsigned long sort_i;
extern unsigned long sort_j;
extern unsigned long sort_k;
extern unsigned long sort_i;
extern unsigned long sort_j;
extern unsigned long sort_i;
void sort(unsigned long n)
{
	unsigned long  ir=n,l=1;

	int jstack=0;
	int flag;
	int a;


	flag = 0;
	for (;;) {
		if (ir-l < M) {
			for (sort_j=l+1;sort_j<=ir;sort_j++) {
				a=arr[sort_j];
				for (sort_i=sort_j-1;sort_i>=l;sort_i--) {
					if (arr[sort_i] <= a) break;
					arr[sort_i+1]=arr[sort_i];
				}
				arr[sort_i+1]=a;
			}
			if (jstack == 0) break;
			ir=istack[jstack--];
			l=istack[jstack--];
		} else {
			sort_k=(l+ir) >> 1;
			SWAP(arr[sort_k],arr[l+1])
			if (arr[l] > arr[ir]) {
				SWAP(arr[l],arr[ir])
			}
			if (arr[l+1] > arr[ir]) {
				SWAP(arr[l+1],arr[ir])
			}
			if (arr[l] > arr[l+1]) {
				SWAP(arr[l],arr[l+1])
			}
			sort_i=l+1;
			sort_j=ir;
			a=arr[l+1];
			for (;;) {
				sort_i++; while (arr[sort_i] < a) sort_i++;
				sort_j--; while (arr[sort_j] > a) sort_j--;
				if (sort_j < sort_i) break;
				SWAP(arr[sort_i],arr[sort_j]);
			}
			arr[l+1]=arr[sort_j];
			arr[sort_j]=a;
			jstack += 2;

			if (ir-sort_i+1 >= sort_j-l) {
				istack[jstack]=ir;
				istack[jstack-1]=sort_i;
				ir=sort_j-1;
			} else {
				istack[jstack]=sort_j-1;
				istack[jstack-1]=l;
				l=sort_i;
			}
		}
	}
}

main()
{
	int index;
	klee_make_symbolic(&index,sizeof(index),"index");

  sort(index);
}

