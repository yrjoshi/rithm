Alias Set Tracker: 6 alias sets for 7 pointer values.
  AliasSet[0x3681980, 1] must alias, Mod       Pointers: (i32* %retval, 4)
  AliasSet[0x36819d0, 4] may alias, Mod/Ref   Pointers: (i32* @main_n, 4), (i32* @main_Fnew, 4), (i32* @main_Fold, 4), (i32* @main_i, 4)
  AliasSet[0x36812b0, 1] must alias, Mod/Ref    forwarding to 0x36819d0
  AliasSet[0x3682360, 1] must alias, Mod/Ref    forwarding to 0x36819d0
  AliasSet[0x36823f0, 1] must alias, Mod/Ref   Pointers: (i32* %temp, 4)
  AliasSet[0x3681220, 1] must alias, Mod/Ref   Pointers: (i32* %ans, 4)
