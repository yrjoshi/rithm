# 1 "/home/yogi/rvtool/test/example/sqrt/sqrt.c"
# 1 "/home/yogi/rvtool/test/example/sqrt/sqrt.c" 1
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 153 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/home/yogi/rvtool/test/example/sqrt/sqrt.c" 2
# 44 "/home/yogi/rvtool/test/example/sqrt/sqrt.c"
double fabs(double n)
{
  double f;

  if (n >= 0) f = n;
  else f = -n;
  return f;
}

float sqrt(val)
float val;
{
  float x = val/10;

  float dx;

  double diff;
  double min_tol = 0.00001;

  int i, flag;

  flag = 0;
  if (val == 0 ) x = 0;
  else {
    for (i=1;i<20;i++)
      {
 if (!flag) {
   dx = (val - (x*x)) / (2.0 * x);
   x = x + dx;
   diff = val - (x*x);
   if (fabs(diff) <= min_tol) flag = 1;
 }
 else
   x =x;
      }
  }
  return (x);
}

int main(void)
{
  float x=1234;
  sqrt(x);
  return 0;
}
